/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/04 20:22:14 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/05 18:11:36 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H

# include "init.h"

int		swap(t_pile **pile);
int		rotate(t_pile **pile);
int		reverse_rotate(t_pile **pile);
int		push(t_pile **a, t_pile **b);
int		ss(t_pile **a, t_pile **b);
int		rr(t_pile **a, t_pile **b);
int		rrr(t_pile **a, t_pile **b);

#endif
