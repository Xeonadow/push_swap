/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 16:13:37 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/12 19:37:16 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "init.h"

# define SA 1
# define SB 2
# define SS 3
# define PA 4
# define PB 5
# define RA 6
# define RB 7
# define RR 8
# define RRA 9
# define RRB 10
# define RRR 11

typedef struct	s_set
{
	t_pile	*p[2];
	int		len;
	int		nbrec;
	int		wk;
	int		st;
}				t_set;

int				sort(t_set *s, int len, t_pile **instruct, int chg);
void			quicksort(t_set *s, size_t len, t_pile **instruct);
void			randomsort(t_set *s, int len, t_pile **instruct);
void			three_sort(t_set *s, t_pile *p, t_pile **instruct);

int				cmp(int nb1, int nb2, int pile);
t_pile			*get_last(t_pile *a);
int				get_pile_len(t_pile *a);
int				get_shorter(t_pile **ret, int size);
int				go_to_from(t_set *s, int i, int j, t_pile **instruct);

void			add_instruct(t_pile **instruct, int data);
void			merge_instruct(t_pile **instruct, t_pile *new);
void			print_instruction(int nbr);

int				swap(t_pile **pile, t_pile **instruct, int data);
int				rotate(t_pile **pile, t_pile **instruct, int data);
int				reverse_rotate(t_pile **pile, t_pile **instruct, int data);
int				push(t_pile **a, t_pile **b, t_pile **instruct, int data);
int				ss(t_pile **a, t_pile **b, t_pile **instruct);
int				rr(t_pile **a, t_pile **b, t_pile **instruct);
int				rrr(t_pile **a, t_pile **b, t_pile **instruct);

#endif
