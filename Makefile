#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/26 12:50:44 by lpousse           #+#    #+#              #
#    Updated: 2016/10/18 18:43:47 by lpousse          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = push_swap

PUSH_SWAP_NAME = push_swap.c quicksort.c tools.c instruct.c operations_ps.c double_op_ps.c randomsort.c small_list_sort.c
CHECKER_NAME = checker.c operations.c double_op.c
INIT_NAME = init.c

SRC_NAME = $(INIT_NAME) $(PUSH_SWAP_NAME) $(CHECKER_NAME)
PUSH_SWAP_PATH = ./push_swap_src/
CHECKER_PATH = ./checker_src/
INIT_PATH = ./init_src/
OBJ_PATH = obj/
INC_PATH = ./includes/ libft/includes/

CC = gcc
CFLAGS = -Wall -Wextra -Werror
CPPFLAGS = $(addprefix -I ,$(INC_PATH))
LDFLAGS = -L libft
LDLIBS = -lft
VPATH = ./init_src ./push_swap_src ./checker_src

OBJ_NAME = $(SRC_NAME:.c=.o)
SRC = $(addprefix $(PUSH_SWAP_PATH),$(PUSH_SWAP_NAME)) $(addprefix $(CHECKER_PATH),$(CHECKER_NAME)) $(addprefix $(INIT_PATH),$(INIT_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
INIT = $(addprefix $(OBJ_PATH),$(INIT_NAME:.c=.o))
PUSH_SWAP = $(addprefix $(OBJ_PATH),$(PUSH_SWAP_NAME:.c=.o))
CHECKER = $(addprefix $(OBJ_PATH),$(CHECKER_NAME:.c=.o))

all: mklib $(NAME)

$(NAME): $(OBJ)
	$(CC) $(LDFLAGS) $(LDLIBS) $(INIT) $(PUSH_SWAP) -o push_swap
	$(CC) $(LDFLAGS) $(LDLIBS) $(INIT) $(CHECKER) -o checker

mklib:
	make -C libft

$(OBJ_PATH)%.o: %.c $(addsuffix *.h,$(INC_PATH))
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

clean:
	rm -fv $(OBJ)
	@rmdir $(OBJ_PATH) 2> /dev/null || true

fclean: clean
	rm -fv $(NAME) checker

libclean: fclean
	make -C libft fclean

wclean:
	rm $(PUSH_SWAP_PATH)*~ $(CHECKER_PATH)*~ $(INIT_PATH)*~ $(addsuffix *~,$(INC_PATH))

re: fclean all

relib: libclean all

norme:
	norminette $(SRC) ./includes/*.h

.PHONY: all clean fclean re mklib norme
