/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quicksort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 17:36:33 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/12 16:39:48 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	median_of_three(int nb1, int nb2, int nb3)
{
	if (nb1 < nb2)
	{
		if (nb3 <= nb1)
			return (nb1);
		else if (nb2 < nb3)
			return (nb2);
	}
	else
	{
		if (nb1 < nb3)
			return (nb1);
		else if (nb3 <= nb2)
			return (nb2);
	}
	return (nb3);
}

static int	choose_pivot(t_pile *a, int len)
{
	int		i;
	int		first;
	int		middle;
	int		last;

	first = a->nbr;
	i = 1;
	while (i < len)
	{
		if (i == (len + 1) / 2)
			middle = a->nbr;
		a = a->next;
		i++;
	}
	last = a->nbr;
	return (median_of_three(first, middle, last));
}

static int	sub_quicksort(t_set *s, size_t len, t_pile **instruct, int pivot)
{
	int		i;
	size_t	j;

	i = 0;
	j = 0;
	while (j < len)
	{
		if ((s->p[s->wk])->nbr == pivot)
		{
			push(s->p + s->st, s->p + s->wk, instruct, PA + s->st);
			rotate(s->p + s->st, instruct, RA + s->st);
		}
		else if (cmp((s->p[s->wk])->nbr, pivot, s->wk))
		{
			push(s->p + s->st, s->p + s->wk, instruct, PA + s->st);
			i++;
		}
		else
			rotate(s->p + s->wk, instruct, RA + s->wk);
		j++;
	}
	return (i);
}

void		quicksort(t_set *s, size_t len, t_pile **instruct)
{
	int		i;
	int		pivot;

	*instruct = NULL;
	pivot = choose_pivot(s->p[s->wk], len);
	i = sub_quicksort(s, len, instruct, pivot);
	sort(s, i, instruct, 1);
	pivot = 0;
	while (pivot++ < i)
		push(s->p + s->wk, s->p + s->st, instruct, PA + s->wk);
	reverse_rotate(s->p + s->st, instruct, RRA + s->st);
	push(s->p + s->wk, s->p + s->st, instruct, PA + s->wk);
	go_to_from(s, 0, len - i - 1, instruct);
	sort(s, len - 1 - i, instruct, 0);
}
