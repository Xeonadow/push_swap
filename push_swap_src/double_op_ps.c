/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   double_op.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 17:48:50 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/05 17:26:05 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ss(t_pile **a, t_pile **b, t_pile **instruct)
{
	add_instruct(instruct, SS);
	return (swap(a, instruct, -1) + swap(b, instruct, -1));
}

int		rr(t_pile **a, t_pile **b, t_pile **instruct)
{
	add_instruct(instruct, RR);
	return (rotate(a, instruct, -1) + rotate(b, instruct, -1));
}

int		rrr(t_pile **a, t_pile **b, t_pile **instruct)
{
	add_instruct(instruct, RRR);
	return (reverse_rotate(a, instruct, -1) + reverse_rotate(b, instruct, -1));
}
