/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instruct.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/27 18:40:31 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/05 18:10:19 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	add_instruct(t_pile **instruct, int data)
{
	t_pile	*new;
	t_pile	*tmp;

	new = (t_pile *)malloc(sizeof(t_pile));
	new->nbr = data;
	new->next = NULL;
	if (*instruct == NULL)
		*instruct = new;
	else
	{
		tmp = *instruct;
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = new;
	}
}

void	merge_instruct(t_pile **instruct, t_pile *new)
{
	t_pile	*tmp;

	if (*instruct == NULL)
		*instruct = new;
	else
	{
		tmp = get_last(*instruct);
		tmp->next = new;
	}
}

void	print_instruction(int nbr)
{
	if (nbr == SA)
		ft_putendl("sa");
	else if (nbr == SB)
		ft_putendl("sb");
	else if (nbr == SS)
		ft_putendl("ss");
	else if (nbr == PA)
		ft_putendl("pa");
	else if (nbr == PB)
		ft_putendl("pb");
	else if (nbr == RA)
		ft_putendl("ra");
	else if (nbr == RB)
		ft_putendl("rb");
	else if (nbr == RR)
		ft_putendl("rr");
	else if (nbr == RRA)
		ft_putendl("rra");
	else if (nbr == RRB)
		ft_putendl("rrb");
	else if (nbr == RRR)
		ft_putendl("rrr");
}
