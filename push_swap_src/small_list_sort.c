/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   small_list_sort.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/12 16:57:05 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/12 17:34:52 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	three_sort(t_set *s, t_pile *p, t_pile **instruct)
{
	int		top;
	int		btm;

	*instruct = NULL;
	top = ft_min(ft_min(p->nbr, (p->next)->nbr), ((p->next)->next)->nbr);
	btm = ft_max(ft_max(p->nbr, (p->next)->nbr), ((p->next)->next)->nbr);
	if (s->wk == 1)
		ft_swap(&top, &btm);
	if (p->nbr == top && ((p->next)->next)->nbr != btm)
	{
		reverse_rotate(s->p + s->wk, instruct, RRA + s->wk);
		swap(s->p + s->wk, instruct, SA + s->wk);

	}
	else if (((p->next)->next)->nbr == top && p->nbr != btm)
		reverse_rotate(s->p + s->wk, instruct, RRA + s->wk);
	else if (((p->next)->next)->nbr == btm && p->nbr != top)
		swap(s->p + s->wk, instruct, SA + s->wk);
	else if (p->nbr == btm && (p->next)->nbr == top)
		rotate(s->p + s->wk, instruct, RA + s->wk);
	else if (p->nbr == btm && ((p->next)->next)->nbr == top)
	{
		swap(s->p + s->wk, instruct, SA + s->wk);
		reverse_rotate(s->p + s->wk, instruct, RRA + s->wk);
	}
}
