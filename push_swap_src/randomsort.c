/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   randomsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/12 18:39:32 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/12 19:57:26 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_pile		*find_longest_sorted(t_pile *p, t_pile *last, int len, int wk)
{
	t_pile	*with;
	t_pile	*without;
	int		i;

	i = -1;
	while (++i < len && last != NULL && p != NULL && cmp(last->nbr, p->nbr, wk))
		p = p->next;
	if (p == NULL || i >= len)
	{
		return (NULL);
	}
	with = NULL;
	add_instruct(&with, p->nbr);
	with->next = find_longest_sorted(p->next, with, len - i - 1, wk);
	without = find_longest_sorted(p->next, last, len - i - 1, wk);
	if (get_pile_len(without) <= get_pile_len(with))
	{
		free_pile(&without);
		return (with);
	}
	free_pile(&with);
	return (without);
}

static int	find_unsorted(t_set *s, t_pile *sorted, int *nbp, t_pile **instr)
{
	t_pile	*p;
	int		i;
	int		j;

	p = s->p[s->wk];
	i = 0;
	j = 0;
	while (j < s->len - *nbp)
	{
		if (sorted != NULL && p->nbr == sorted->nbr)
		{
			p = p->next;
			sorted = sorted->next;
			j++;
		}
		else
		{
			p = p->next;
			i = go_to_from(s, j, i, instr);
			push(s->p + s->st, s->p + s->wk, instr, PA + s->st);
			(*nbp)++;
		}
	}
	return (i);
}

static int	find_place(t_set *s, int to_place, int i, int len)
{
	t_pile	*p;
	int		tlen;
	int		j;

	p = s->p[s->wk];
	tlen = get_pile_len(p);
	j = -1;
	while (++j < len - i && cmp(to_place, p->nbr, s->wk))
		p = p->next;
	if (j > 0)
		return (j + i);
	while (i + j < tlen)
	{
		p = p->next;
		j++;
	}
	while (j < tlen)
	{
		if (cmp(p->nbr, to_place, s->wk))
			return (j - tlen + i);
		p = p->next;
		j++;
	}
	return (i);
}

void		randomsort(t_set *s, int len, t_pile **instruct)
{
	int		i;
	int		j;
	int		nbpush;
	t_pile	*sorted;

	nbpush = 0;
	sorted = find_longest_sorted(s->p[s->wk], NULL, len, s->wk);
	j = s->len;
	s->len = len;
	i = find_unsorted(s, sorted, &nbpush, instruct);
	free_pile(&sorted);
	s->len = j;
	j = 0;
	while (nbpush-- > 0)
	{
		j = find_place(s, (s->p[s->st])->nbr, i, len - nbpush - 1);
		i = go_to_from(s, j, i, instruct);
		push(s->p + s->wk, s->p + s->st, instruct, PA + s->wk);
	}
	go_to_from(s, 0, i, instruct);
}
