/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert_sort.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/04 17:35:36 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/12 19:36:46 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	find_insert_place(t_set *s, int i)
{
	t_pile	*work;
	int		to_place;
	int		len;
	int		k;

	work = s->p[s->wk];
	to_place = (s->p[s->st])->nbr;
	len = get_pile_len(work);
	k = 0;
	while (k < len - i)
	{
		work = work->next;
		k++;
	}
	k = 0;
	while (cmp(to_place, work->nbr, s->wk))
	{
		work = work->next;
		k++;
	}
	return (k);
}

int			go_to_from(t_set *s, int i, int j, t_pile **instruct)
{
	int		fwd;
	int		bwd;

	if (i > j)
	{
		fwd = i - j;
		bwd = j + get_pile_len(s->p[s->wk]) - i;
	}
	else
	{
		fwd = i + get_pile_len(s->p[s->wk]) - j;
		bwd = j - i;
	}
	if (fwd <= bwd)
		while (fwd-- > 0)
			rotate(s->p + s->wk, instruct, RA + s->wk);
	else
		while (bwd-- > 0)
			reverse_rotate(s->p + s->wk, instruct, RRA + s->wk);
	return (i);
}
/*
static int	find_sorted_sequence(t_set *s, size_t len)
{
	t_pile	*work;
	int		sorted_len;
	int		sorted_pos;
	int		tmp_len;
	size_t	i;

	i = 0;
	work = s->p[s->wk];
	sorted_len = 1;
	while(i < len - 1)
	{
		tmp_len = 1;
		while (i + tmp_len < len && cmp((work->next)->nbr, work->nbr, s->wk))
		{
			tmp_len++;
			work = work->next;
		}
		if (tmp_len > sorted_len)
		{
			sorted_pos = i;
			sorted_len = tmp_len;
		}
		work = work->next;
		i += tmp_len;
	}
}
*/
void		insert_sort(t_set *s, size_t len, t_pile **instruct)
{
	size_t	i;
	size_t	j;
	t_pile	*tmp;

	*instruct = NULL;
	if ((s->p[s->wk])->nbr > ((s->p[s->wk])->next)->nbr)
		swap(s->p + s->wk, instruct, SA + s->wk);
	i = 2;
	j = 0;
	while (i < len)
	{
		j = go_to_from(s, i, j, instruct);
		tmp = get_last(s->p[s->wk]);
		if (cmp(tmp->nbr, (s->p[s->wk])->nbr, s->wk))
		{
			push(s->p + s->st, s->p + s->wk, instruct, PA + s->st);
			j = go_to_from(s, find_insert_place(s, i), j, instruct);
			push(s->p + s->wk, s->p + s->st, instruct, PA + s->wk);
		}
		i++;
	}
	j = go_to_from(s, 0, j, instruct);
}
