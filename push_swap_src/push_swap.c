/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 16:13:18 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/12 19:55:22 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		check_sort(t_pile *a, t_pile *b)
{
	if (b != NULL)
		return (-1);
	while (a != NULL && a->next != NULL)
	{
		if (a->nbr >= (a->next)->nbr)
			return (-1);
		a = a->next;
	}
	return (0);
}

static int		check_sort_part(t_pile *p, int len, int pile)
{
	int		i;

	i = 1;
	while (i < len)
	{
		if (cmp(p->nbr, (p->next)->nbr, pile))
			return (-1);
		p = p->next;
		i++;
	}
	return (0);
}

static t_pile	*choose_sort(t_set *s, int len)
{
	t_pile	*ret;

	ret = NULL;
	if (len == 3 && get_pile_len(s->p[s->wk]) == 3)
		three_sort(s, s->p[s->wk], &ret);
	else if (len < 30)
		randomsort(s, len, &ret);
	else
		quicksort(s, len, &ret);
	return (ret);
}

int				sort(t_set *s, int len, t_pile **instruct, int chg)
{
	t_pile	*ret;
	int		tmpwk;

	tmpwk = (s->wk + chg) % 2;
	if (len <= 1 || check_sort_part(s->p[tmpwk], len, tmpwk) == 0)
		return (0);
	(s->nbrec)++;
	if (chg == 1)
		ft_swap(&(s->wk), &(s->st));
	if (len == 2 && cmp((s->p[s->wk])->nbr, ((s->p[s->wk])->next)->nbr, s->wk))
		swap(s->p + s->wk, instruct, SA + s->wk);
	else if (len > 2)
	{
		ret = choose_sort(s, len);
		merge_instruct(instruct, ret);
	}
	if (chg == 1)
		ft_swap(&(s->wk), &(s->st));
	(s->nbrec)--;
	return (0);
}

int				main(int ac, char **av)
{
	t_set	set;
	t_pile	*instruct;
	t_pile	*tmp;

	if (ac <= 1)
		return (0);
	(set.p)[0] = init_a(ac, av);
	(set.p)[1] = NULL;
	instruct = NULL;
	set.len = get_pile_len((set.p)[0]);
	set.wk = 0;
	set.st = 1;
	set.nbrec = 0;
	if (check_sort((set.p)[0], (set.p)[1]) < 0)
		sort(&set, set.len, &instruct, 0);
	free_pile(set.p + 0);
	tmp = instruct;
	while (tmp != NULL)
	{
		print_instruction(tmp->nbr);
		tmp = tmp->next;
	}
	free_pile(&instruct);
	return (0);
}
