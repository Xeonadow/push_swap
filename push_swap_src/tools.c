/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 17:44:59 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/11 17:12:36 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		cmp(int nb1, int nb2, int pile)
{
	if (pile == 0)
		return (nb1 > nb2);
	return (nb1 < nb2);
}

int		go_to_from(t_set *s, int i, int j, t_pile **instruct)
{
	int		fwd;
	int		bwd;

	if (i > j)
	{
		fwd = i - j;
		bwd = j + get_pile_len(s->p[s->wk]) - i;
	}
	else
	{
		fwd = i + get_pile_len(s->p[s->wk]) - j;
		bwd = j - i;
	}
	if (fwd <= bwd)
		while (fwd-- > 0)
			rotate(s->p + s->wk, instruct, RA + s->wk);
	else
		while (bwd-- > 0)
			reverse_rotate(s->p + s->wk, instruct, RRA + s->wk);
	return (i);
}

int		get_pile_len(t_pile *a)
{
	int len;

	len = 0;
	while (a != NULL)
	{
		a = a->next;
		len++;
	}
	return (len);
}

int		get_shorter(t_pile **ret, int size)
{
	int		min;
	int		tmp;
	int		i;
	int		imin;

	i = 0;
	min = -1;
	while (i < size)
	{
		tmp = get_pile_len(ret[i]);
		if (min < 0 || tmp < min)
		{
			min = tmp;
			imin = i;
		}
		i++;
	}
	return (imin);
}

t_pile	*get_last(t_pile *a)
{
	while (a->next != NULL)
		a = a->next;
	return (a);
}
