/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 15:53:43 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/12 16:31:40 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int	do_op(char *str, t_pile **a, t_pile **b)
{
	if (ft_strcmp("sa", str) == 0)
		swap(a);
	else if (ft_strcmp("sb", str) == 0)
		swap(b);
	else if (ft_strcmp("ss", str) == 0)
		ss(a, b);
	else if (ft_strcmp("pa", str) == 0)
		push(a, b);
	else if (ft_strcmp("pb", str) == 0)
		push(b, a);
	else if (ft_strcmp("ra", str) == 0)
		rotate(a);
	else if (ft_strcmp("rb", str) == 0)
		rotate(b);
	else if (ft_strcmp("rr", str) == 0)
		rr(a, b);
	else if (ft_strcmp("rra", str) == 0)
		reverse_rotate(a);
	else if (ft_strcmp("rrb", str) == 0)
		reverse_rotate(b);
	else if (ft_strcmp("rrr", str) == 0)
		rrr(a, b);
	else
		error();
	return (1);
}

static int	check_sort(t_pile *a, t_pile *b)
{
	if (b != NULL)
	{
		ft_putendl("KO");
		return (-1);
	}
	while (a != NULL && a->next != NULL)
	{
		if (a->nbr >= (a->next)->nbr)
		{
			ft_putendl("KO");
			return (-1);
		}
		a = a->next;
	}
	ft_putendl("OK");
	return (0);
}

int			main(int ac, char **av)
{
	t_pile	*a;
	t_pile	*b;
	char	*line;

	if (ac <= 1)
		return (0);
	a = init_a(ac, av);
	b = NULL;
	while (ft_gnl(0, &line))
	{
		do_op(line, &a, &b);
		ft_strdel(&line);
	}
	ft_strdel(&line);
	check_sort(a, b);
	free_pile(&a);
	free_pile(&b);
	return (0);
}
