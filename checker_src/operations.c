/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operations.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 16:25:42 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/05 17:28:20 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

int		swap(t_pile **pile)
{
	t_pile	*tmp;

	if (*pile != NULL && (*pile)->next != NULL)
	{
		tmp = (*pile)->next;
		(*pile)->next = tmp->next;
		tmp->next = *pile;
		*pile = tmp;
		return (1);
	}
	return (0);
}

int		rotate(t_pile **pile)
{
	t_pile	*first;
	t_pile	*last;

	if (*pile != NULL && (*pile)->next != NULL)
	{
		last = (*pile)->next;
		while (last->next != NULL)
			last = last->next;
		last->next = *pile;
		first = *pile;
		*pile = first->next;
		first->next = NULL;
		return (1);
	}
	return (0);
}

int		reverse_rotate(t_pile **pile)
{
	t_pile	*prev;
	t_pile	*last;

	if (*pile != NULL && (*pile)->next != NULL)
	{
		last = *pile;
		while (last->next != NULL)
		{
			prev = last;
			last = last->next;
		}
		prev->next = NULL;
		last->next = *pile;
		*pile = last;
		return (1);
	}
	return (0);
}

int		push(t_pile **a, t_pile **b)
{
	t_pile	*tmp;

	if (*b == NULL)
		return (0);
	tmp = *b;
	*b = tmp->next;
	tmp->next = *a;
	*a = tmp;
	return (1);
}
