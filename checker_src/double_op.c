/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   double_op.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 17:48:50 by lpousse           #+#    #+#             */
/*   Updated: 2016/10/05 17:29:02 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

int		ss(t_pile **a, t_pile **b)
{
	return (swap(a) + swap(b));
}

int		rr(t_pile **a, t_pile **b)
{
	return (rotate(a) + rotate(b));
}

int		rrr(t_pile **a, t_pile **b)
{
	return (reverse_rotate(a) + reverse_rotate(b));
}
