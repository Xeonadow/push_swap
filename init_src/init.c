/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 20:08:10 by lpousse           #+#    #+#             */
/*   Updated: 2016/09/26 18:05:22 by Xeonadow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "init.h"

int			error(void)
{
	ft_putendl_fd("Error", 2);
	exit(0);
	return (-1);
}

void		free_pile(t_pile **pile)
{
	t_pile	*tmp;

	tmp = NULL;
	while (*pile != NULL)
	{
		tmp = (*pile)->next;
		free(*pile);
		*pile = tmp;
	}
}

static int	add_nbr(t_pile **a, int nbr)
{
	t_pile	*new;
	t_pile	*tmp;

	new = (t_pile *)malloc(sizeof(t_pile));
	new->nbr = nbr;
	new->next = NULL;
	if (*a == NULL)
		*a = new;
	else
	{
		tmp = *a;
		if (nbr == tmp->nbr)
			error();
		while (tmp->next != NULL)
		{
			tmp = tmp->next;
			if (nbr == tmp->nbr)
				error();
		}
		tmp->next = new;
	}
	return (0);
}

static int	ft_atoi_error(char *str)
{
	int		i;
	long	nb;
	int		neg;

	i = 0;
	nb = 0;
	neg = 0;
	if (str[i] == '-' || str[i] == '+')
	{
		neg = (str[i] == '-');
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		if ((nb * 10 + str[i] - 48) > (long)INT_MAX + neg)
			error();
		nb = nb * 10 + str[i] - 48;
		i++;
	}
	if (str[i] != '\0' || ft_isdigit(str[i - 1]) == 0)
		error();
	return (neg ? -nb : nb);
}

t_pile		*init_a(int nb, char **list)
{
	t_pile	*a;
	char	**tab;
	int		i;
	int		j;

	a = NULL;
	i = 1;
	while (i < nb)
	{
		tab = ft_strsplit(list[i], ' ');
		j = 0;
		while (tab[j])
		{
			add_nbr(&a, ft_atoi_error(tab[j]));
			ft_strdel(tab + j);
			j++;
		}
		free(tab);
		i++;
	}
	return (a);
}
